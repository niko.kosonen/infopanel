port module Main exposing (..)

import Browser
import Debug exposing (log)
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as Decode exposing (field, int, maybe, string)



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- PORTS


port sendMessage : String -> Cmd msg


port messageReceiver : (String -> msg) -> Sub msg



-- MODEL


type alias ProducerData =
    { message : String
    }


type alias ConsumerData =
    { id : Int
    , message : String
    , status : Int
    }


type alias Producer =
    { message : String
    , messages : List String
    }


type alias Consumer =
    { id : Int
    , message : String
    , messages : List String
    , status : Int
    }


type alias JSONMessage =
    { consumer : Maybe ConsumerData
    , producer : Maybe ProducerData
    }


type alias Model =
    { cid : Int
    , consumers : Dict Int Consumer
    , producer : Producer
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model
        0
        (Dict.fromList
            [ ( 1, Consumer 1 "" [] 0 )
            , ( 2, Consumer 2 "" [] 0 )
            , ( 3, Consumer 3 "" [] 0 )
            , ( 4, Consumer 4 "" [] 0 )
            ]
        )
        (Producer "" [])
    , Cmd.none
    )



-- UPDATE


type Msg
    = Recv String
    | Toggle Int
    | ToggleServer


updateConsumer : Consumer -> Maybe Consumer -> Maybe Consumer
updateConsumer new old =
    case old of
        Nothing ->
            Nothing

        Just consumer ->
            Just (Consumer new.id new.message (List.take 10 (new.message :: consumer.messages)) new.status)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Recv message ->
            let
                json =
                    parseJSON message

                c =
                    case json.consumer of
                        Nothing ->
                            Consumer 0 "" [] 0

                        Just consumerData ->
                            Consumer consumerData.id consumerData.message [] consumerData.status

                p =
                    case json.producer of
                        Nothing ->
                            model.producer

                        Just producerData ->
                            Producer producerData.message (List.take 10 (producerData.message :: model.producer.messages))
            in
            ( { model
                | consumers = Dict.update c.id (updateConsumer c) model.consumers
                , producer = p
              }
            , Cmd.none
            )

        Toggle cid ->
            ( model, sendMessage ("toggle" ++ String.fromInt cid) )

        ToggleServer ->
            ( model, sendMessage "toggleserver" )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    messageReceiver Recv



-- VIEW


renderMessage : String -> Html Msg
renderMessage message =
    li [] [ text <| message ]


renderTopMessages : List String -> Html Msg
renderTopMessages messages =
    ul [] (List.map renderMessage messages)


buttonStatus : Int -> String
buttonStatus status =
    case status of
        1 ->
            "disconnect"

        0 ->
            "connect"

        _ ->
            "waiting..."


getConsumer : Dict Int Consumer -> Int -> Consumer
getConsumer consumers cid =
    case Dict.get cid consumers of
        Nothing ->
            Consumer 0 "" [] 0

        Just consumer ->
            consumer


view : Model -> Html Msg
view model =
    div [ class "app" ]
        [ div [ class "producer" ]
            [ div [ class "caption" ] [ text "Producer" ]
            , button [ class "button disabled", onClick ToggleServer ]
                [ text "disconnect" ]
            , renderTopMessages model.producer.messages
            ]
        , div
            [ class "consumers" ]
            [ div [ class "group" ]
                [ div [ class "caption" ] [ text "group 1" ]
                , div [ class "clients" ]
                    [ div [ class "client" ]
                        [ div [ class "caption" ] [ text ("Consumer : " ++ String.fromInt (getConsumer model.consumers 1).id) ]
                        , button [ class "button", onClick (Toggle 1) ] [ text (buttonStatus (getConsumer model.consumers 1).status) ]
                        , renderTopMessages (getConsumer model.consumers 1).messages
                        ]
                    , div [ class "client" ]
                        [ div [ class "caption" ] [ text ("Consumer : " ++ String.fromInt (getConsumer model.consumers 2).id) ]
                        , button [ class "button", onClick (Toggle 2) ] [ text (buttonStatus (getConsumer model.consumers 2).status) ]
                        , renderTopMessages (getConsumer model.consumers 2).messages
                        ]
                    ]
                ]
            , div [ class "group" ]
                [ div [ class "caption" ] [ text "group 2" ]
                , div [ class "clients" ]
                    [ div [ class "client" ]
                        [ div [ class "caption" ] [ text ("Consumer : " ++ String.fromInt (getConsumer model.consumers 3).id) ]
                        , button [ class "button", onClick (Toggle 3) ] [ text (buttonStatus (getConsumer model.consumers 3).status) ]
                        , renderTopMessages (getConsumer model.consumers 3).messages
                        ]
                    , div [ class "client" ]
                        [ div [ class "caption" ] [ text ("Consumer : " ++ String.fromInt (getConsumer model.consumers 4).id) ]
                        , button [ class "button", onClick (Toggle 4) ] [ text (buttonStatus (getConsumer model.consumers 4).status) ]
                        , renderTopMessages (getConsumer model.consumers 4).messages
                        ]
                    ]
                ]
            ]
        ]


consumerDecoder : Decode.Decoder ConsumerData
consumerDecoder =
    Decode.map3 ConsumerData (field "id" int) (field "message" string) (field "status" int)


producerDecoder : Decode.Decoder ProducerData
producerDecoder =
    Decode.map ProducerData (field "message" string)


messageDecoder : Decode.Decoder JSONMessage
messageDecoder =
    Decode.map2 JSONMessage
        (maybe (field "consumer" consumerDecoder))
        (maybe (field "producer" producerDecoder))


parseJSON : String -> JSONMessage
parseJSON msg =
    case Decode.decodeString messageDecoder msg of
        Err err ->
            log (Debug.toString err)
                { consumer = Nothing
                , producer = Nothing
                }

        Ok result ->
            result
